+++
title =  "AI and why is it bad"
authors = ["oomfie"]
date =   "2024-01-15"
description = "Rambling of internet creature"

[extra]
images = "media/ai.webp"
embed_images = ["/media/ai.webp"]

[taxonomies]
categories = ["AI"]
tags = ["Bite-size"]
+++

![](/media/ai.webp)

Artificial intelligence or better known as Machine learning is a tech that you see everywhere marketed to your average person.

AI is a marketing term since as at the time of writing, there's is no such thing as a "true AI"

So why does AI get so much hate?

One of the reasons i do see being mentioned frequently on the internet is that AI is used to cut down on labor costs or steal copyrighted meterial or spreading misinfomation.

So to me, "AI" or machine learning as i like to call it, is a tool and tools get misused by users often. But, there are cases for AI, just not for replacing jobs or stealing copyrighted material.
