+++
title =  "Building a Custom NixOS ISO with Github Actions"
authors = ["oomfie"]
date =   "2024-07-21"
description = "How to Abuse github actions to build your NixOS ISOs"

[extra]
images = "media/nixiso_gitact.webp"
embed_images = ["/media/nixiso_gitact.webp"]
disclaimer = """
- This Blog Post does not tell how u can compile your own NixOS ISO for that i recommend watching this video before trying to do this: https://youtu.be/-G8mN6HJSZE
"""

[taxonomies]
categories = ["AI"]
tags = ["Bite-size"]
+++

![](/media/nixiso_gitact.webp)


Building a Custom NixOS can be tiring on your local machine so why not do it with the Power of github actions!

Step 1:

create a github repo then do the following:

```yaml
# create a .github/workflows/iso_build file
# add the following lines

name: "Build ISO"
on:
  workflow_dispatch:
  workflow_call:

  push:
    paths-ignore: # don't rebuild if only documentation has changed
      - "**.md"

concurrency:
  group: ${{ github.workflow }}-${{ github.ref || github.run_id }}-iso
  cancel-in-progress: true
jobs:
  tests:
    runs-on: ubuntu-latest
    steps:
    - name: Checkout Repo
      uses: actions/checkout@v4
    - name: install nix
      uses: DeterminateSystems/nix-installer-action@main
    - name: nix cache
      uses: DeterminateSystems/magic-nix-cache-action@main
    - name: nix flake checker
      uses: DeterminateSystems/flake-checker-action@main
      #with:
      #github_access_token: ${{ secrets.GITHUB_TOKEN }}
    - run: nix build .#iso
    - name: Upload ISO as artifact
      id: upload
      uses: actions/upload-artifact@v4
      with:
          name: ISO
          path: |
            ./result/iso/*.iso
          if-no-files-found: error
          retention-days: 0
          compression-level: 0

```

then it will build it for u and post it as artifact
