+++
title = "Why nobody should use discord as platform for information"
authors = ["oomfie"]
description = "the reason why discord sucks at being as platform for information"
date = "2023-06-11"
updated = "2023-06-22"

[extra]
images = "media/example1.webp"
embed_images = ["/media/example1.webp"]

[taxonomies]
categories = ["Rants"]
tags = ["Discord"]
+++

![](/media/example1.webp)
[a wayback machine screenshot of catppuccin's wallpapers repo on march 29th 2023](https://web.archive.org/web/20230329181811/https://github.com/catppuccin/wallpapers)

Recently, I have seen FOSS projects replacing repos with Discord and it isn't good and this is why: 

1: Discord servers/invite links aren't index-able in search engine.

2: You cant access those multimedia without creating an account first. (and verifying it with a phone number if that Discord server has that as option)

3: Discord isn't designed as a multimedia sharing platform. (i.e messages or media will get lost to time if the user deletes the messages or media or have their account deleted)

4: Discord's search feature does not work half the time. (i.e a message you sent 3 days ago will appear but a message you sent a week ago will not appear)

5: Do not use discord as the only place for announcements or early builds of software for testing.

An alternative to this is not using Discord for any of that.

Its understandable for using Discord as community platform only and thats it.
