<div align="center">
  <h1 align="center">Rewrite V7</h1>
</div>

<div align="center">
  <a href="https://gitlab.com/oomfie/oomfie.gitlab.io/raw/production/Licenses/COPYING.AGPL-V3">
    <img src="https://img.shields.io/static/v1?label=License&message=AGPL-3&color=1e1e20&style=flat-square">
  </a>
  <br>
  <hr />
</div>

## Install
Be sure to have all [you need](https://www.getzola.org/documentation/getting-started/installation/) before running anything.

[check the getstarted guide](https://www.getzola.org/documentation/getting-started/cli-usage/)
